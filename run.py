import argparse
from os import path
import numpy as np
from psychopy import prefs
prefs.hardware["audioLib"] = ["pygame"]
from psychopy import core, visual, logging, sound
from psychopy.hardware import keyboard
from psychopy_bids import bids

parser = argparse.ArgumentParser(
    description="Run the experiment")
parser.add_argument(
    dest="subject_id",
    metavar="subject_id",
    help="Subject identifier"
)
parser.add_argument(
    dest="mode",
    choices=["sim_test", "scanner_test", "experiment"],
    metavar="mode",
    help="The mode in which the experiment is running, either sim_test, scanner_test, or experiment"
)

args = parser.parse_args()

mode = {
    "sim_test": {
        "screen_setting": False,
        "dimensions": (800, 600)
    },
    "scanner_test": {
        "screen_setting": False,
        "dimensions": (800, 600)
    },
    "experiment": {
        "screen_setting": False,
        "dimensions": (800, 600)
    }

}

class ExperimentControl():
    """
    
    """

    def __init__(self, window):
        self.window = window
        self.clock = core.Clock()
        self.keyboard = keyboard.Keyboard()

class PresentObject():
    def __init__(self, experiment, log_information):
                self.experiment = experiment
                self.log_information = log_information

    def present(self):
        pass

    def log_event(self, onset, duration):
        print(self.log_information)

    def log_keypress(self):
        keys = self.experiment.keyboard.getKeys()
        if 'q'  in keys:
            end_experiment = 1
            core.quit()
        
        for key in keys:
            print(key.name, key.rt, key.duration)



class PresentVisObject(PresentObject):

    def __init__(self, duration, experiment, log_information, *stimulus_object):
        super().__init__(experiment=experiment, log_information=log_information)
        self.duration = duration
        self.stimulus_object = stimulus_object
        self.frames = round(self.duration/self.experiment.window.monitorFramePeriod)

    def present(self):
        onset = {}

        for stim in self.stimulus_object:
            stim.autoDraw = True

        experiment.window.logOnFlip("something", level=logging.EXP)
        experiment.window.timeOnFlip(onset, "onset")

        for f in range(self.frames):
            experiment.window.flip()
            self.log_keypress()

        for stim in self.stimulus_object:
            stim.autoDraw = False

        self.log_event(onset["onset"], self.duration)


class PresentAudObject(PresentObject):

    def __init__(self, experiment, log_information, stimulus_object, duration=None):
        super().__init__(experiment=experiment, log_information=log_information)
        self.stimulus_object = stimulus_object
        self.stim_duration = self.stimulus_object.getDuration()
        self.duration = duration or self.stim_duration

    def present(self):
        self.stimulus_object.play()
        onset = core.getTime()
        while core.getTime() < (onset + self.duration):
            self.log_keypress()
        self.stimulus_object.stop()

        self.log_event(onset, self.stim_duration)

class PresentNullObject(PresentObject):
    
    def __init__(self, window, log_information, duration):
        super().__init__(log_information=log_information)
        self.window = window
        self.duration = duration

logfile = logging.LogFile(path.join("logs", "test.log"),
                          level=logging.INFO,
                          filemode="w",
                          encoding="utf8")

win = visual.Window(size=mode[args.mode]["dimensions"],
                    fullscr=mode[args.mode]["screen_setting"],
                    units="pix",
                    checkTiming=True)

experiment = ExperimentControl(win)

def initialize_presentation_objects():
    fixation = visual.TextStim(win,
                               text="+",
                               name="fixation_cross")
    book_image = visual.ImageStim(win,
                                  size=(320, 212),
                                  pos=(0, 0),
                                  name="book_image",
                                  image=path.join('materials', "image", "book.jpg"))
    book_text = visual.TextStim(win,
                                text="book",
                                name="book_word")
    book_audio = sound.Sound(stereo=True,
                             sampleRate=16000,
                             value=path.join("materials", "audio", "book.mp3"),
                             name="book_audio")
    instruction_text = visual.TextStim(win,
                                       text="",
                                       name='block_instruction')

    fixation_present = PresentVisObject(2, experiment, {"something": "something"}, fixation)
    book_image_present = PresentVisObject(2, experiment, {"something": "something"}, book_image)
    book_text_present = PresentVisObject(2, experiment, {"something": "something"}, book_text)
    instruction_text = PresentVisObject(2, experiment, {"something": "something"}, instruction_text)
    book_audio_present = PresentAudObject(experiment, {"something": "something"}, book_audio)

    return {
        "fixation": fixation_present,
        "book_image": book_image_present,
        "book_text": book_text_present,
        "book_audio": book_audio_present,
        "instruction_text": instruction_text
    }


presentation_objects = initialize_presentation_objects()

block_instruction = ["Please press button when you see a picture of a book.",
                     "Please press button when you see the word book.",
                     "Please press button when you hear the word book."]

order = [["book_image", "book_audio", "book_text"],
         ["book_text", "book_audio", "book_image"],
         ["book_audio", "book_image", "book_text"]]


for block in range(3):
    presentation_objects["instruction_text"].stimulus_object[0].text = block_instruction[block]
    presentation_objects["instruction_text"].present()

    for trial in range(3):
        presentation_objects["fixation"].present()
        presentation_objects[order[block][trial]].present()

win.close()
